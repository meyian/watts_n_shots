Rails.application.routes.draw do
  
  get 'about_us', to: "about_us#show"
  
  # upload
  match 'admin/drinks', via: [:get, :post]
  match 'admin/speakers', via: [:get, :post]


  # event
  post 'event/create'

  get 'event/update/:id/:step', to: "event#update_get"
  patch 'event/update/:id',     to: "event#update_patch"

  get 'event/view/:id',         to: "event#read"
  delete 'event/delete/:id',    to: "event#delete"

  post 'event/payment',        to: "event#payment"

  # user
  get 'user/cancel_event/:id', to: "user#cancel_event"
  get 'user/profile_page', as: :get_profile




  # steps
  namespace :steps do    
    # get 'step1_speakers', as: :get_speakers
    match 'step1_speakers', as: :get_speakers, via: [:get, :post]

    get 'step2_drinks', as: :get_drinks
    get 'step3_total', as: :get_total
    get 'step5_location', as: :get_location
    get 'step6_review', as: :get_review
    get 'step7_payment', as: :get_payment
    get 'step8_confirmation', as: :get_confirmation
  end

  root 'application#homepage'
  
  # devise routes
  devise_for :users, path: '', 
    controllers: { registrations: 'auth/registrations' }, 
    path_names: { 
      sign_in: 'login', 
      sign_out: 'logout', 
      password: 'password', 
      confirmation: 'confirmation', 
      unlock: 'unlock', 
      registration: 'register', 
      sign_up: 'join' }
    
  
  # add a GET logout
  devise_scope :user do
    get 'logout', to: 'devise/sessions#destroy'
    get 'profile/edit', to: 'devise/registrations#edit', as: :get_profile_edit
    get 'steps/step4_login_register'=> 'auth/register#step4_login_register', as: :steps_get_logged_in
    post 'steps/step4_login'=> 'auth/login#create', as: :post_auth_login
    post 'steps/step4_register'=> 'auth/register#create', as: :post_auth_register
    post 'password-level'=> 'auth/register#password_meter', as: :post_auth_password_level
        
    # redirection links alias
    get 'steps/step4_login'=> 'auth/register#step4_login_register'
    get 'steps/step4_register'=> 'auth/register#step4_login_register'
  end
  
  # redirect all missing path to home
  get "*path", to: redirect('/')

end

