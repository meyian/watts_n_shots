class AddSettingToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :setting, :string
  end
end
