class AddPaymentTypeToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :payment_type, :string
  end
end
