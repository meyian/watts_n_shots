class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :location
      t.datetime :time
      t.json :drinks
      t.json :speaker

      t.timestamps
    end
  end
end
