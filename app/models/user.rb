class User < ApplicationRecord
  
  devise :database_authenticatable, :async, :registerable, :confirmable, :timeoutable,
         :recoverable, :rememberable, :validatable, :lockable, :trackable
       
 
  validates :name, :presence => true
  validates :phone, numericality: { only_integer: true }
end
