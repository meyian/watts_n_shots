class Event < ApplicationRecord
  
  after_save :send_order_email, if: :new_saved_record?

  def get_cookie_hash_string

    puts "#{self.id}"

    self.get_cookie_hash.to_json.to_s.html_safe
  end

  def get_cookie_hash
    _h_cookie_format = {
      drinks:   self.drinks,
      location: self.location,
      speaker:  self.speaker,
      time: {
        string:    self.time.strftime("%d/%m/%Y %I:%M %p"), 
        timestamp: self.time.strftime("%s")
      },
      total:    self.get_total,
      user_id:  self.user_id
    }
  end

  # private
  
  def get_total
    _arr_speaker = [{
      item: "Speaker Package '#{self.speaker['name']}'",
      quantity: 1,
      subtotal: self.speaker["price"],
    }]

    _arr_drinks = self.get_drinks_subtotal_data
    _arr_total = _arr_speaker + _arr_drinks

    puts "_arr_total: #{_arr_total}"

#    _int_total = _arr_total.reduce(0) { |prev, curr| prev += curr["subtotal"].to_i}
    _int_total= 0
    _arr_total.each{ |row| _int_total += row[:subtotal].to_i}
    _arr_total.push({
      item: "Total",
      quantity: "",
      subtotal: _int_total
    })
  end

  def get_drinks_subtotal_data
    _arr_drinks = []
    h_drinks = self.drinks

    if !h_drinks.blank?

      h_drinks.each do |k1, h_drink|

        h_items = h_drink["items"]

        h_items.each do |k2, h_item|

          _int_quantity = h_item["quantity"].to_i

          if _int_quantity > 0
            _arr_drinks.push ({
              item:     h_item["name"],
              quantity: _int_quantity,
              subtotal: _int_quantity * h_item["price"].to_i,
            })
          end

        end
      end
    end
    _arr_drinks
  end
  
  def get_time_period
    hours = self.time.hour
    if hours <= 10
      "morning"
    elsif hours <= 14 
      "afternoon"
    elsif hours > 17 
      "evening"
    end
  end
    
  def new_saved_record?
    created_at == updated_at
  end
  
  def send_order_email
    CustomMailer.new_order(self).deliver_now
  end
  handle_asynchronously :send_order_email
  
  def send_order_receipt_email
    CustomMailer.order_receipt(self).deliver_now
  end
  handle_asynchronously :send_order_receipt_email
  
end

__END__

function getTotalData(objCookieData){

  var arrRetData  = null;
  objCookieData   = objCookieData || getCookieObj();

  if (validateCookieData(objCookieData)){
    arrRetData = [];
    var intTotal = 0;

    // speaker
    arrRetData.push({
      item: `Speaker Package '${objCookieData.speaker.name}'`,
      quantity: 1,
      subtotal: +objCookieData.speaker.price,
    });

    var intSpeakerTotal = +objCookieData.speaker.price;

    // drinks
    var arrDrinksData = getDrinksSubtotalData(objCookieData.drinks);
    var intDrinksTotal = arrDrinksData.reduce((prev, curr) => (prev + curr.subtotal), 0);
    


    arrRetData = [...arrRetData, ...arrDrinksData];
    intTotal = intDrinksTotal + intSpeakerTotal;

    console.log("intDrinksTotal: ", intDrinksTotal);
    console.log("intSpeakerTotal: ", intSpeakerTotal);

    arrRetData.push({
      item: "Total",
      quantity: "",
      subtotal: intTotal
    });
  }

  return arrRetData;
}

----

function getDrinksSubtotalData(objDrinksData){
  objDrinksData = objDrinksData || (getCookieObj() ? getCookieObj().drinks : null);
  var arrRetData = [];

  for (var i in objDrinksData){
    var objDrink = objDrinksData[i];

    for (var j in objDrink.items){
      var objDrinkItem = objDrink.items[j];

      var strDrink = objDrinkItem.name;
      var intSubtotal = +objDrinkItem.price * +objDrinkItem.quantity;

      if (intSubtotal != 0){
        arrRetData.push({
          item: capitalize(strDrink),
          quantity: objDrinkItem.quantity,
          subtotal: intSubtotal
        });
      }
    }
  }

  return arrRetData;
}