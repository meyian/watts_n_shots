class MyMailer < Devise::Mailer   
  helper :application # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  default template_path: 'devise/mailer' # to make sure that your mailer uses the devise views
  default from: 'Orders@wattsnshots.com'
  layout 'mailer'
  before_action :set_logo_attachement
  
  private
  
  def set_logo_attachement
    attachments.inline['logo.png'] = File.
          read("#{Rails.root}/app/assets/images/wns.logo.png")
  end

end