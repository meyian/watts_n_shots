class ApplicationMailer < ActionMailer::Base
  default from: 'Orders@wattsnshots.com'
  layout 'mailer'
end
