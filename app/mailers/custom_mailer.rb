class CustomMailer < ApplicationMailer
  
  before_action :set_logo_attachement
 
  def new_order(event)
    @event = event
    subject = "New order"
    mail(to: 'Orders@wattsnshots.com', subject: subject)
  end
  
  def order_receipt(event)
    @event = event
    @user = User.find_by_id(@event.user_id)
    subject = "Order receipt"
    mail(to: @user.email, subject: subject)
  end
  
  private
  
  def set_logo_attachement
    attachments.inline['logo.png'] = File.
          read("#{Rails.root}/app/assets/images/wns.logo.png")
  end
end