require "uri"
require 'json'
require 'net/https'
require 'open-uri'

class EventController < ApplicationController
  
  before_action :authenticate_user!
  protect_from_forgery prepend: true
  
  def create
    puts "EventController > create"
    # puts "#{params}"

    h_drinks      = params[:data][:drinks]
    h_speaker     = params[:data][:speaker]
    str_location  = params[:data][:location]
    str_time      = params[:data][:time][:timestamp]
    dt_time       = DateTime.strptime(str_time,'%s')
    str_user_id   = current_user.id

    h_create      = {
      location: str_location,
      time:     dt_time,
      drinks:   h_drinks, 
      speaker:  h_speaker,
      user_id:  str_user_id
    }

    str_status = Event.create(h_create)

    render json: {status: str_status}
  end

  def update_patch
    _int_event_id     = params[:id].to_i

    puts "event controller > update_patch | _int_event_id: #{_int_event_id.to_s}"

    h_drinks      = params[:data][:drinks]
    h_speaker     = params[:data][:speaker]
    str_location  = params[:data][:location]
    str_time      = params[:data][:time][:timestamp]
    dt_time       = DateTime.strptime(str_time,'%s')

    h_update      = {
      location: str_location,
      time:     dt_time,
      drinks:   h_drinks, 
      speaker:  h_speaker
    }

    puts "update_patch | h_update: #{h_update.to_s}"

    _event = Event.find_by_id(_int_event_id)
    _str_status = _event.update h_update

    render json: {status: _str_status}
    
  end

  def update_get

    _str_id     = params[:id]
    _int_step   = params[:step].to_i

    puts "_str_id: #{_str_id}"
    puts "_int_step: #{_int_step}"

    _event      = Event.find_by_id(_str_id)
    _cookie_str = _event.get_cookie_hash_string

    _h_step_num_to_string = {
      1 => "step1_speakers",
      2 => "step2_drinks",
      3 => "step3_total",
      4 => "step4_login_register",
      5 => "step5_location",
      6 => "step6_review",
      7 => "step7_payment",
      8 => "step8_confirmation"
    }

    _h_step_num_to_data = {
      1 => WattsNShots::Utils.get_speaker_data.to_json.to_s.html_safe,
      2 => WattsNShots::Utils.get_drinks_data.to_json.to_s.html_safe
    }

    _str_step_url = "steps/#{_h_step_num_to_string[_int_step]}"
    _str_data = _h_step_num_to_data[_int_step]
    
    puts "_str_data: #{_str_data.to_s}"
    puts "_str_step_url: #{_str_step_url.to_s}"

    _h_locals = {
      is_user_logged_in: user_signed_in?,
      step: _int_step,
      current_page: "step_#{_int_step}",
      event_id: _str_id,
      cookie_string: _cookie_str,
      mode: :edit_mode,
      data: _str_data,
    }

    # puts "event controller > update > _h_locals: #{_h_locals}"

    render :template => _str_step_url, locals: _h_locals
  end

=begin

Things to do:

  * Create a new column for indoors/outdoors
  * Save this payment type data in the front end

=end


  def create_event(h_wattsnshots)
    h_drinks      = h_wattsnshots[:drinks]
    h_speaker     = h_wattsnshots[:speaker]
    str_location  = h_wattsnshots[:location]
    str_time      = h_wattsnshots[:time][:timestamp]
    dt_time       = DateTime.strptime(str_time,'%s')
    str_setting   = h_wattsnshots[:in_or_out]
    str_payment_type   = h_wattsnshots[:payment_type]
    str_user_id   = current_user.id

    h_create      = {
      location: str_location,
      time:     dt_time,
      drinks:   h_drinks, 
      speaker:  h_speaker,
      setting: str_setting,
      payment_type: str_payment_type,
      user_id:  str_user_id
    }

    Event.create(h_create) 
  end

  def get_interpay_hash(h_interpay)
    h_interpay = {
      app_id: 2452016049,
      app_key: h_interpay["app_key"],
      order_id: h_interpay["order_id"],
      currency: h_interpay["currency"],
      amount: h_interpay["amount"],
      order_desc: h_interpay["order_desc"],
      return_url: h_interpay["return_url"]
    }
  end

  def interpay

  end

  def cash_on_delivery

  end

  def payment

    puts "event_controller > interpay | params['data']: #{params['data']}"
    
    _h_interpay = params['data']['interpay']
    _h_wattsnshots = params['data']['wattsnshots']

    _str_response = ""

    if !_h_interpay.blank?

      @event = create_event(_h_wattsnshots)

      _h_interpay = get_interpay_hash(_h_interpay)
      _h_interpay[:order_id] = @event.id
      
      url = URI.parse('https://test.interpayafrica.com/interapi/ProcessPayment')
      
      req = Net::HTTP::Post.new(url.path)
      req.form_data = _h_interpay.to_h
      # req.basic_auth url.user, url.password if url.user
      con = Net::HTTP.new(url.host, url.port)
      con.use_ssl = true
      res = con.start {|http| http.request(req)}  

      _str_response = res.body

    else

      @event = create_event(_h_wattsnshots)
      _str_response = true

    end

    render json: {status: 200, response: _str_response}

  end

  def read

    _str_id     = params[:id]
    _event      = Event.find(_str_id)

    puts "event: #{_event.speaker}"

    _cookie_str = _event.get_cookie_hash.to_json.to_s.html_safe

    render :template => "steps/step6_review", locals: {
      is_user_logged_in: user_signed_in?,
      event_id: _str_id,
      event: _event,
      cookie_string: _cookie_str,
      mode: :read_mode,
      current_page: "step_6"
    }
  end

  def delete

    _str_id     = params[:id]
    _event      = Event.find_by_id(_str_id)
    
    str_status  = _event.update_attribute(:cancelled, DateTime.now);

    render json: {status: str_status}
  end
end
