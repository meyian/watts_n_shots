#require '/Users/abudu_h/Documents/_business/watts_n_shots/code/watts_n_shots/lib/wattsnshots/utils.rb'

class StepsController < ApplicationController
  
  before_action :authenticate_user!, :only => [:step5_location, :step6_payment,
    :step7_review, :step8_confirmation]
  protect_from_forgery prepend: true, :only => [:step5_location, :step6_payment,
    :step7_review, :step8_confirmation]
  
  def step1_speakers
    puts "StepsController > step1_speakers | data: #{request.body.to_s}"

    _data = WattsNShots::Utils.get_speaker_data.to_json.to_s.html_safe

    render locals: {is_user_logged_in: user_signed_in?, data: _data, current_page: "step_1", event_id: nil}
  end
  
  def step2_drinks

    _data = WattsNShots::Utils.get_drinks_data.to_json.to_s.html_safe

    render locals: {is_user_logged_in: user_signed_in?, data: _data, current_page: "step_2"}
  end
  
  def step3_total

    render locals: {is_user_logged_in: user_signed_in?, current_page: "step_3"}
  end
  
  def step4_login_register
    if user_signed_in?
      redirect_to action: :step5_location
    end

    render locals: {is_user_logged_in: user_signed_in?, current_page: "step_4"}
  end
  
  def step5_location

    render locals: {is_user_logged_in: user_signed_in?, current_page: "step_5"}
  end
  
  def step6_review
    puts "steps_controller > step6_review"
    render locals: {is_user_logged_in: user_signed_in?, current_page: "step_6"}
  end
  
  def step7_payment

    render locals: {is_user_logged_in: user_signed_in?, current_page: "step_7"}
  end
  
  def step8_confirmation

    render locals: {is_user_logged_in: user_signed_in?, current_page: "step_8"}
  end
  
  private

    def get_drinks_data
      get_drinks_data_real
      # get_drinks_data_dummy
    end

    def get_speaker_data
      get_speaker_data_dummy
    end

    def get_drinks_data_file_contents
      _str_file_location = get_drinks_data_file_location
      _str_file_data = File.read _str_file_location
    end

    def get_drinks_format_file_contents
      _str_contents = get_drinks_data_file_contents
      _h_drinks = {}

      _arr_file_contents = _str_contents.split "\n"
      _int_drink_counter = 0
      _int_item_counter = 0

      _arr_file_contents.each do |str_line|
        puts "str_line: #{str_line}"
        _arr_line_contents = str_line.split ","
        

        if _arr_line_contents.length == 1

          _int_item_counter = 0
          _int_drink_counter += 1

          _h_drinks[_int_drink_counter] =  {
            id: _int_drink_counter,
            drink_type: str_line,
            items: Hash.new
          }
        else
          _int_item_counter += 1
          _arr_items_data = str_line.split ","

          puts "_h_drinks: #{_h_drinks}"

          _h_drinks[_int_drink_counter][:items][_int_item_counter] = {
            id: _int_item_counter,
            name: _arr_items_data[0],
            price: _arr_items_data[1],
            quantity: 0
          }
        end
      end
      
      _h_drinks
    end

    def get_drinks_data_real
      get_drinks_format_file_contents
    end

    def get_drinks_data_file_location
      Rails.root.join('public', 'uploads', 'drinks.csv')
    end

    def get_speaker_data_file_location
      Rails.root.join('public', 'uploads', 'speakers.csv')
    end

    def get_speaker_data_dummy
      {
        1 => {
          id: 1,
          name: "A",
          range: "0 - 10",
          price: 100,
          checked: false,
        },
        2 => {
          id: 2,
          name: "B",
          range: "10 - 20",
          price: 200,
          checked: false,
        },
        3 => {
          id: 3,
          name: "C",
          range: "20 - 30",
          price: 300,
          checked: false,
        }
      };
    end

end

__END__

0-10,A,100
10-20,B,200
20-30,C,300 
30-40,D,400