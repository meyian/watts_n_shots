class ApplicationController < ActionController::Base
  layout :layout_by_resource
  
  before_action :configure_permitted_parameters, if: :devise_controller?

  def homepage

    puts "ApplicationController > homepage"

    render locals: {is_user_logged_in: user_signed_in?}
  end
  
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :phone])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :phone])
  end
  
  private

  def layout_by_resource
    if devise_controller?
      "devise"
    else
      "application"
    end
  end
  
  def authorize_admin
    redirect_to root_url unless current_user.is_admin
  end


end
