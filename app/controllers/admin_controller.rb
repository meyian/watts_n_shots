class AdminController < ApplicationController
  
  before_action :authenticate_user!
  protect_from_forgery prepend: true
  before_action :authorize_admin

  def drinks

    if params[:authenticity_token]

      uploaded_io = params[:drinks_csv]
      save_file(uploaded_io, "drinks.csv")
      
      render "success"
    end

  end

  def speakers

    if params[:authenticity_token]

      uploaded_io = params[:speaker_csv]
      save_file(uploaded_io, "speakers.csv")
      
      render "success"
    end
  end

  private

    def save_file(obj_source, str_dest_name)

      File.open(Rails.root.join('public', 'uploads', str_dest_name), 'wb') do |file|
        file.write(obj_source.read)
      end

    end

end

__END__

https://robm.me.uk/ruby/2014/01/25/pstore.html
http://www.mattmorgante.com/technology/csv

