class UserController < ApplicationController

  before_action :authenticate_user!
  protect_from_forgery prepend: true
  
  def cancel_event

    _str_id     = params[:id]
    _event      = Event.find_by_id(_str_id)
    _cookie_str = _event.get_cookie_hash_string

    puts "_cookie_str: #{_cookie_str}";

    render :template => "steps/step6_review", locals: {
      is_user_logged_in: user_signed_in?,
      event_id: _str_id,
      event: _event,
      cookie_string: _cookie_str,
      mode: :delete_mode,
      current_page: "step_6"
    }
  end

  def profile_page

    # Get list of events
    _int_user_id = current_user.id
    _arr_future_event_data = []
    _arr_past_event_data = []

    if _int_user_id
      _rcd_events  = Event.where({user_id: _int_user_id, cancelled: nil})

      _rcd_events.each do |evt|

        _int_event_id = evt.id
        _h_event_data = {
          id: _int_event_id,
          timestamp: evt.time.strftime("%s").to_i,
          date: evt.time.strftime("%B %d, %Y %I:%M %p"),
          edit_url: "/event/update/#{_int_event_id}/1",
          view_url: "/event/view/#{_int_event_id}",
          cancel_url: "/user/cancel_event/#{_int_event_id}"
        }
        
        if evt.time > DateTime.now
          _arr_future_event_data.push _h_event_data
        else
          _arr_past_event_data.push _h_event_data
        end
      end
    end
    
    _arr_future_event_data = _arr_future_event_data.sort_by {|x| x[:timestamp]}
    _arr_past_event_data = _arr_past_event_data.sort_by {|x| x[:timestamp]}

    puts "_arr_future_event_data: #{_arr_future_event_data}"
    puts "_arr_past_event_data: #{_arr_past_event_data}"

    @capitalized_name = capitalize current_user.name
    @future_data = _arr_future_event_data.to_json.to_s.html_safe
    @past_data = _arr_past_event_data.to_json.to_s.html_safe

    render locals: {is_user_logged_in: user_signed_in?}
  end

  private
  def capitalize(str_phrase)
    str_phrase.slice(0,1).capitalize + str_phrase.slice(1..-1)
  end
end
