class Auth::RegistrationsController < Devise::RegistrationsController
 
  protected

  # The path used after sign up.
  def after_update_path_for(resource)
    get_profile_path
  end

end
