class Auth::RegisterController < Devise::RegistrationsController
  
  layout "application"
  
  def step4_login_register
    if user_signed_in?
      redirect_to steps_get_location_path
    else
      @register_tab = false
      # register
      build_resource
      
      # login
      @resource_sign_in = resource_class.new(sign_in_params)
      clean_up_passwords(@resource_sign_in)
      
      yield resource if block_given?
      respond_with(resource, @resource_sign_in, serialize_options(@resource_sign_in))
    end
  end
  
  def create
    @register_tab = true
    super do |resource|
      if !resource.persisted?
        clean_up_passwords resource
        set_minimum_password_length
        
        # login
        @resource_sign_in = resource_class.new(sign_in_params)
        clean_up_passwords(@resource_sign_in)
        
        respond_with resource do |format|
          format.html {render :action => :step4_login_register and return}
        end
      end
    end
  end
  
  def password_meter
    password_level = 'very weak'
    if !params[:password].nil? && !params[:password].blank?
      puts params[:password]
      checker = StrongPassword::StrengthChecker.new(params[:password])
      entropy = checker.calculate_entropy(use_dictionary: true)
      if entropy < 3
        password_level = 'very weak'
      elsif entropy >= 3 && entropy < 7
        password_level = 'weak'
      elsif entropy >= 7 && entropy < 15
        password_level = 'good'
      elsif entropy >= 15 && entropy < 25
        password_level = 'strong'
      elsif entropy >= 24
        password_level = 'very strong'
      end
    end
    
    render :json => {password_level: password_level}, status: 200
  end
  
  protected
  
  # register
  def after_sign_up_path_for(resource)
    steps_get_location_path
  end
  
  
  
  # login
  def sign_in_params
    devise_parameter_sanitizer.sanitize(:sign_in)
  end
  
  def serialize_options(resource)
    methods = resource_class.authentication_keys.dup
    methods = methods.keys if methods.is_a?(Hash)
    methods << :password if resource.respond_to?(:password)
    { methods: methods, only: [:password] }
  end
  
end
