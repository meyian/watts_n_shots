class Auth::LoginController < Devise::SessionsController
  
  protected
  
  def auth_options
    { :recall => 'auth/register#step4_login_register', :scope => :user }
  end
  
  def signed_in_root_path(resource_or_scope)
    steps_get_location_path
  end
  
end
