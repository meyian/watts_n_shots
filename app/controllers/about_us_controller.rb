class AboutUsController < ApplicationController
  def show
    render locals: {is_user_logged_in: user_signed_in?, current_page: "about_us", event_id: nil}
  end
end
