import React from "react"
import PropTypes from "prop-types"
class FutureEventsTable extends React.Component {

  getTbodyContent(){

    var strProps = this.props.data;
    var arrProps = JSON.parse(strProps);
    var arrTrProps = arrProps.map((x, i) => (
<tr key={i}>
  <td><a href={x.view_url} target="_blank" className="a-view-link"><h6>{x.date}</h6></a></td>
  <td><a href={x.edit_url} target="_blank" className="a-edit-button"><button className="btn-edit-button">Edit</button></a></td>
  <td><a href={x.cancel_url} target="_blank" className="a-cancel-button"><button className="btn-cancel-button">Cancel Event</button></a></td>
</tr>
));

    return arrTrProps;
  }

  render () {

    var strProps = this.props.data;
    var arrProps = JSON.parse(strProps);
    var jsxOutput = "";

    if (arrProps.length){

    var jsxOutput = (
<div>
  <h3 id="toc_5">Future Events</h3>
  <table>
    <thead>
      <tr>
          <th>Event Date</th>
          <th></th>
          <th></th>
      </tr>
    </thead>
    <tbody>
      {this.getTbodyContent.call(this)}
      <tr className="tr-end-block"><td colSpan="3">&nbsp;</td></tr>
    </tbody>
  </table>
</div>
    )

  }

    return jsxOutput;
  }
}

export default FutureEventsTable

{/*  */}