import React from "react";


class TotalsTable extends React.Component {

  getTableRows(){
    var arrItems = getTotalData();
    var arrJsxRows = arrItems.map((x, i) => {

      var strItemRow = x.item;
      var strSubtotalRow = `GHS ${x.subtotal}`;

      if (x.item.toLowerCase() == "total"){
        var arrItems = [strItemRow, strSubtotalRow];

        arrItems = arrItems.map((x) => <h6>{x}</h6>);
        [strItemRow, strSubtotalRow] = arrItems;
      }

      

return (<tr key={i}>
  <td>{strItemRow}</td>
  <td>{x.quantity}</td>
  <td>{strSubtotalRow}</td>
</tr>);

    });

    return arrJsxRows;
  }

  render () {

    var jsxRows = this.getTableRows();

    return (
<div className="div-total-table-wrapper">
  <table className="tbl-total-table">
    <thead>
      <tr>
          <th>Item</th>
          <th>Quantity</th>
          <th>Subtotal</th>
      </tr>
    </thead>
    <tbody>
      {jsxRows}
      <tr className="tr-end-block"><td colSpan="3">&nbsp;</td></tr>
    </tbody>
  </table>
</div>
    );
  }
}

export default TotalsTable
