import React from "react"
import PropTypes from "prop-types"

class PastEventsTable extends React.Component {
  render () {

    var jsxOutput = '';

    

    var strData = this.props.data;
    var arrData = JSON.parse(strData);

    if (arrData.length){

      var arrTrs = arrData.map((x, i) => (
  <tr key={i}>
    <td><a href={x.view_url} target="_blank"><h6>{x.date}</h6></a></td>
  </tr>
      ));

      jsxOutput = 
(
<div>
<h3 id="toc_6">Past Events</h3>
<table className="tbl-past">
  <thead>
    <tr>
      <th>Event Date</th>
    </tr>
  </thead>
  <tbody>
    {arrTrs}
    <tr className="tr-end-block"><td>&nbsp;</td></tr>
  </tbody>
</table>
</div>
);

    }

    return jsxOutput;
  }
}

export default PastEventsTable

/*  */