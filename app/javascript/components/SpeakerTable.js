import React from "react"

class SpeakerTable extends React.Component {

  constructor(props){
    super(props);
    this.state = {checked: {}};
  }

  savePropDataToState(objData){
    var objDataCpy = {...objData};
    delete objData.checked;
    this.setState({checked: objDataCpy});
  }

  tableTDataRows(objData, index){

    var boolIsCheckedFromState = this.state.checked.id == objData.id; 
    var boolIsCheckedFromProps = objData.checked;
    var boolIsChecked = boolIsCheckedFromState || boolIsCheckedFromProps;

    var strObjData = JSON.stringify(objData);

    if (boolIsCheckedFromProps){
      this.savePropDataToState(objData);
      this.saveStateToInput();
    }

    var arrTdData = [
      <td><input type="radio" obj-data={strObjData} onChange={this.newSelection.bind(this)} name="rdo-user-choice" id={objData.id} checked={boolIsChecked} /></td>,
      <td>{objData.range}</td>,
      <td><h5>{objData.name}</h5>
      <span className="span-info" id={"span-info-index-"+index}><i className="fas fa-info-circle"></i></span>
      </td>,
      <td>GHS {objData.price}</td>
    ];

    return arrTdData;
  }

  newSelection(evt){

    
    var strData = $(evt.target).attr('obj-data');

    var objData = JSON.parse(strData);
    delete objData['checked'];

          
    this.setState({checked: objData});
    this.saveStateToInput(objData);
  }

  saveStateToInput(objData){
    objData = objData || this.state.checked;
    var strData = JSON.stringify(objData);

    
    $("#inp-txt-output").val(strData);
  }

  tableBodyRows(arrData){

    var self = this;

    var jsxTRows = arrData.map((item, index) => {

      var arrTD = this.tableTDataRows.call(self, item, index);
      var strKeyName = `${index}.${item.name}`

      
      return (
        <tr key={strKeyName}>
          {arrTD}
        </tr>
      )
    })

    let jsxNoneRow = (
    <tr>
      <td><input type="radio" id="rdo-none" name="rdo-user-choice" /></td>
      <td>--</td>
      <td><h5>None</h5></td>
      <td>GHS 0</td>
    </tr>);

    return (
<tbody>
  {jsxNoneRow}
  {jsxTRows}
  <tr className="tr-end-block"><td colSpan="4">&nbsp;</td></tr>
</tbody>
    );
  }

  getPropsAsObject(){
    var strPropsData = this.props.data;
    var objData = JSON.parse(strPropsData);

    return objData;
  }

  renderTable(){

    var jsxTHead = (
<thead>
  <tr>
    <th>Your Choice</th>
    <th>Number of People Attending</th>
    <th>Suggested Speaker Package</th>
    <th>Price</th>
  </tr>
</thead>
    );

    var objData = this.getPropsAsObject();
    var arrData = Object.values(objData);
    var jsxTableBody = this.tableBodyRows.call(this, arrData);

    return (
<table className="tbl-speaker-table">
  {jsxTHead}
  {jsxTableBody}
</table>
    );
  }

  render () {
    return (
<div className="div-speaker-table-wrapper">
  {this.renderTable.call(this)}
  <input type="hidden" id="inp-txt-output"/>
</div>
    );
  }
}

export default SpeakerTable
