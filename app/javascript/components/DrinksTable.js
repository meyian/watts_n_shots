import React from "react"

class DrinksTable extends React.Component {

  constructor(props){
    super(props);
    this.state = {};
  }

  getDrinkTypeBlock(objData, intIndexOne){

    var strDrinkName    = objData.drink_type;
    var strDrinkNumber  = objData.id;
    var objItemData     = objData.items;
    var arrItemData     = Object.values(objItemData);

    var that = this;

    var arrJsxItemsRows = arrItemData.map((objData, intIndex) => {

      var strItemId = objData.id;
      var strKey = `${strDrinkNumber}.${strItemId}`;
      var strDataQuantity = objData.quantity;
      
      if (this.state[strKey] == undefined){
        this.state[strKey] = strDataQuantity;
      }

      return (
      <tr key={strKey} data-drink_type={strDrinkNumber}>
        <td>{objData.name}</td>
        <td>GHS {objData.price}</td>
        <td><input type="text" onChange={this.setQuantity.bind(that)} size="3" value={this.state[strKey]} id={strKey} item-number={intIndex} /></td>
      </tr>
      )}
    );
    

    var arrJsxTrs = [
  <tr key={intIndexOne} drink-number={strDrinkNumber}>
    <td colSpan="3"><center><h6>{strDrinkName}</h6></center></td>
  </tr>, ...arrJsxItemsRows];
    
    return arrJsxTrs;
  }

  setData(intDrinkNumber, intItemId, intNewQuantity){

    var strData = $("#inp-txt-output").val() || $("#inp-txt-data").val();
    var objData = JSON.parse(strData);
    
    var objNewData = {...objData};

    objNewData[intDrinkNumber].items[intItemId].quantity = intNewQuantity;

    var strNewData = JSON.stringify(objNewData);

    $("#inp-txt-output").val(strNewData);

  }

  setQuantity(evt){

    var strKey = evt.target.id;
    var arrParts = strKey.split('.');
    var intDrinkNumber = +arrParts[0];
    var intItemId = +arrParts[1];
    var intNewQuantity = evt.target.value;

    this.setData(intDrinkNumber, intItemId, intNewQuantity);

    this.setState({[strKey]: intNewQuantity});
  }

  render() {

    var jsxTableHeader = (
      <tr>
        <th>Drink</th>
        <th>Price</th>
        <th>Number</th>
      </tr>
    );

    var that = this;

    var objCookieData = getCookieObj();

    var boolCanUseCookie = objCookieData.hasOwnProperty("drinks") && objCookieData.drinks;
    var objData = boolCanUseCookie ? objCookieData.drinks : JSON.parse(this.props.data);

    var arrData = Object.values(objData);
    var arrJsxDrinkTypeRows = arrData.map(this.getDrinkTypeBlock.bind(that));

    return (
      <div className="div-drinks-table-wrapper">
        <table className="tbl-drinks-table">
          <thead>
            {jsxTableHeader}
          </thead>
          <tbody>
            {arrJsxDrinkTypeRows}
            <tr className="tr-end-block"><td colSpan="3">&nbsp;</td></tr>
          </tbody>
        </table>
        <input type="hidden" value={this.props.data} id="inp-txt-data"/>
        <input type="hidden" id="inp-txt-output"/>
      </div>
    );
  }
}

export default DrinksTable