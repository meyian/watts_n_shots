var boolHasRunSetup = false;
var intIntervalId;

loadCookieStringIntoCookie();

// todo: remove the loading of the cookie from the string code in getCookieString

function loadCookieStringIntoCookie(){
  var boolRetResponse = false;
  var boolIsReadmode = window.mode && (window.mode == "read_mode");

  if (isCookieDataEmpty() || boolIsReadmode){

    console.log("cookie data came in empty, or is read mode");

    if (doesCookieStringExist()){

      var strCookieName = getCookieName();
      var strValue = cookie_string;
      var intDays = 31;
    
      setCookie(strCookieName, strValue, intDays);
      
      boolRetResponse = true;
    }
  }
  else{
    console.log("cookie data has been filled");
  }

  console.log("loadCookieStringIntoCookie | boolRetResponse: ", boolRetResponse);


  return boolRetResponse;
}

function isUserLegalAge(){
  var strOver18CookieName = "wattsnshots_age_verification";
  var strData = getCookie(strOver18CookieName);
  return !!strData;
}

function getCookieName(){
  var strCookieName = "";

  if (window.mode && (window.mode == "edit_mode")){
    strCookieName = "wattsnshots_editmode";
  }
  else{
    strCookieName = "wattsnshots";
  }

  return strCookieName;
}

function processValidSubmission(){
  // postToPage();
  var boolRetValue = false;

  if (saveDataToCookie()){
    redirectToNextPage();
    boolRetValue = true;
  }

  return boolRetValue;
}

function handleClickContinue(event){
  console.log("handleClickContinue | Clicked continue");

  if (validatePage()){
    processValidSubmission();
  }
  // show error message
  else{
    // var strMsg = "Invalid entry: check your submission";
    // showErrorMessage(strMsg);
  }

  event.preventDefault();
}


function saveDataToCookie(){
  var boolRetValue = false;

  if (doesFunctionExist("getPageData")){
    var objData = getPageData();

    if (objData){
      console.log("saveDataToCookie | objData exists. objData: ", objData);
      boolRetValue = appendDataToCookie(objData);
    }
  }

  return boolRetValue;
}


function getNextPageUrl(){
  var strUrl = document.getElementsByClassName("a-continue")[0].href;
  return strUrl;
}

function redirectToNextPage(){
  var strNextPage = getNextPageUrl();

  window.location.href = strNextPage;
} 

function appendDataToCookie(objNewData){
  var strCookieName = getCookieName();
  var strCookieData = getCookie(strCookieName);
  var objCookieData = strCookieData ? JSON.parse(strCookieData) : {};

  var objNewData = {...objCookieData, ...objNewData};
  
  var strValue = JSON.stringify(objNewData);
  var intDays = 31;

  var arrData = [strCookieName, strValue, intDays];

  setCookie(...arrData);

  return true;
}

function doesFunctionExist(strFunctionName){
  var boolResult = typeof(eval(strFunctionName)) === typeof(Function);
  return boolResult;
} 

function showErrorMessage(strMsg){
  alert(strMsg);
}

function getPostUrlLocation(){
  var strRetUrl = document.location.pathname;
  return strRetUrl;
}

function setCookie(name, value, days) {
  var expires = "";

  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000));
    expires = "; expires=" + date.toUTCString();
  }

  document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function doesCookieStringExist(){

  var boolAnswer = false;

  if ((typeof cookie_string !== "undefined") && cookie_string){
    boolAnswer = true;
  }

  return boolAnswer;
}

function isCookieDataEmpty(){
  var objCookieData = getCookieObj();
  var boolAnswer = false;

  if (Object.keys(objCookieData).length == 0){
    boolAnswer = true;
  }

  return boolAnswer;
}

function getCookie(name){
  name = name || getCookieName();

  var nameEQ = name + "=";
  var ca = document.cookie.split(';');

  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);

    if (c.indexOf(nameEQ) == 0) 
      return c.substring(nameEQ.length,c.length);
  }

  return null;
}

function getCookieObj() {

  var strCookieData = getCookie();
  var objCookieData = strCookieData ? JSON.parse(strCookieData) : {};

  return objCookieData;
}

function eraseEditModeCookie() {
  var strName = "wattsnshots_editmode";
  eraseCookie(strName);
}

function eraseCookie(strName) {
  console.log("eraseCookie");
  var strName   = strName || getCookieName();
  var strValue  = "";
  var intDays   = 31;

  setCookie(strName, strValue, intDays);
  // document.cookie = name+'=; Max-Age=-99999999;';
}

function capitalize(str){
  str = str.split(" ");

  for (var i = 0, x = str.length; i < x; i++) {
      str[i] = str[i][0].toUpperCase() + str[i].substr(1);
  }

  return str.join(" ");
}

function hasLoadedTable(){
  var boolHasLoadedTable = false;

  if (document.getElementsByTagName("tr").length == 1){
    boolHasLoadedTable = false;
  }
  else{
    boolHasLoadedTable = true;
  }

  return boolHasLoadedTable;
}

function setupTableData(){

  if (!hasLoadedTable()){
    document.getElementById("tr-row-0").insertAdjacentHTML("afterend", totalDataToTrs());
  }
  
}

function getDrinksSubtotalData(objDrinksData){
  objDrinksData = objDrinksData || (getCookieObj() ? getCookieObj().drinks : null);
  var arrRetData = [];

  for (var i in objDrinksData){
    var objDrink = objDrinksData[i];

    for (var j in objDrink.items){
      var objDrinkItem = objDrink.items[j];

      var strDrink = objDrinkItem.name;
      var intSubtotal = +objDrinkItem.price * +objDrinkItem.quantity;

      if (intSubtotal != 0){
        arrRetData.push({
          item: capitalize(strDrink),
          quantity: objDrinkItem.quantity,
          subtotal: intSubtotal
        });
      }
    }
  }

  return arrRetData;
}

function getSpeakerSubtotalData(objCookieData){

  objCookieData   = objCookieData || getCookieObj();
  let objSpeakerData = objCookieData.speaker;
  let objRetData = {
    item: `Speaker Package '${objSpeakerData.name}'`,
    quantity: 1,
    subtotal: +objSpeakerData.price,
  }

  return objRetData;
}

function getTotalData(objCookieData){
  
  console.log('getTotalData');

  var arrRetData  = null;
  objCookieData   = objCookieData || getCookieObj();

  if (validateCookieData(objCookieData)){
    arrRetData = [];
    var intTotal = 0;

    // speaker
    let objSpeakerData = getSpeakerSubtotalData(objCookieData);

    console.log("objSpeakerData: ", objSpeakerData);
    console.log("objSpeakerData.subtotal: ", objSpeakerData.subtotal);

    if (objSpeakerData.subtotal != 0)
      arrRetData.push(objSpeakerData);

    var intSpeakerTotal = objSpeakerData.subtotal;

    // drinks
    var arrDrinksData = getDrinksSubtotalData(objCookieData.drinks);
    var intDrinksTotal = arrDrinksData.reduce((prev, curr) => (prev + curr.subtotal), 0);
    
    arrRetData = [...arrRetData, ...arrDrinksData];
    intTotal = intDrinksTotal + intSpeakerTotal;

    arrRetData.push({
      item: "Total",
      quantity: "",
      subtotal: intTotal
    });
  }

  return arrRetData;
}

function totalDataToTrs(objTotalData){

  objTotalData = objTotalData || getTotalData();
  var arrTrs = [];
  var strTrs = null;

  for (var i in objTotalData){

    var objData = objTotalData[i];
    var strTds = "";
    var strTr = "";
    var arrTds = [];

    for (var j in objData){
      var strText = (j == "subtotal") ? "GHS " + objData[j] : objData[j];
      
      if (objData.item == "Total" && strText){
        strText = `<h6>${strText}</h6>`;
      }

      var strTd = `<td>${strText}</td>`;
      arrTds.push(strTd);
    }

    strTds = arrTds.join("\n");
    strTr = ['<tr>', strTds, '</tr>'].join("\n");
    arrTrs.push(strTr);
  }

  if (arrTrs.length){
    strTrs = arrTrs.join("\n");
  }

  return strTrs;
}

function validateCookieData(objData){
  return true;
}

function validateCookieData(objData, intPageNumber){
  return true;
}

function rerunSetupIfNeeded(){
  var objThis = this;

  if (doesFunctionExist("setup")){

    intIntervalId = setTimeout((() => {
      if (!hasRunSetup()){
        setup();
      }
      else{
        clearInterval(intIntervalId);
      }
    }).bind(objThis), 500);
  }
}

function hasRunSetup(){
  return boolHasRunSetup;
}

function toggleHasRunSetup(){
  boolHasRunSetup = true;
}

function setupTimeAndLocation(){
  document.getElementById("spn-time").innerHTML     = getTime();
  document.getElementById("spn-location").innerHTML = getLocation();
  document.getElementById("spn-setting").innerHTML  = capitalize(getIndoorsOutdoors());
}


function getTime(){
  var objCookieData = getCookieObj();
  var boolHasTime = objCookieData.hasOwnProperty("time");

  var strTimeFinal = ""
  
  if (boolHasTime){

    let strDateTime = objCookieData.time.string;

    let strDate = strDateTime.split(' ')[0];
    let strTime = strDateTime.split(' ')[1];

    let strWhen = "";

    switch (strTime){
      case '10:00AM':
        strWhen = "morning";
        break;
      case '2:00PM':
        strWhen = "afternoon";
        break;
      case '5:00PM':
        strWhen = "evening";
        break;
    }

    strTimeFinal = `${strDate} ${strWhen}`;
  }

  return strTimeFinal;
}

function getLocation(){
  var objCookieData = getCookieObj();
  var strLocation = (objCookieData.location || '').toString();

  return strLocation;
}

function getIndoorsOutdoors(){
  var objCookieData = getCookieObj();
  var strLocation = (objCookieData.in_or_out || '').toString();

  return strLocation;
}