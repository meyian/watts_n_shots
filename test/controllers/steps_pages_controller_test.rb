require 'test_helper'

class StepsPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get speakers" do
    get steps_pages_speakers_url
    assert_response :success
  end

  test "should get drinks" do
    get steps_pages_drinks_url
    assert_response :success
  end

end
