require 'test_helper'

class AdminControllerTest < ActionDispatch::IntegrationTest
  test "should get drinks" do
    get admin_drinks_url
    assert_response :success
  end

  test "should get speakers" do
    get admin_speakers_url
    assert_response :success
  end

end
