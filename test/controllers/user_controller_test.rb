require 'test_helper'

class UserControllerTest < ActionDispatch::IntegrationTest
  test "should get cancel_event" do
    get user_cancel_event_url
    assert_response :success
  end

  test "should get profile_page" do
    get user_profile_page_url
    assert_response :success
  end

end
