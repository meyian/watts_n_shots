require 'test_helper'

class EventControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get event_create_url
    assert_response :success
  end

  test "should get update" do
    get event_update_url
    assert_response :success
  end

  test "should get read" do
    get event_read_url
    assert_response :success
  end

  test "should get delete" do
    get event_delete_url
    assert_response :success
  end

end
