module WattsNShots

  module Utils

    class << self

      def get_drinks_data
        get_drinks_data_real
      end
  
      def get_speaker_data
        get_speakers_data_real
      end
  
      def get_data_file_contents(str_mode)
        _str_file_data = ""

        if str_mode == "drinks"
          _str_file_data = File.read get_drinks_data_file_location
        elsif str_mode  == "speakers"
          _str_file_data = File.read get_speaker_data_file_location
        end

        _str_file_data
      end
  
      def get_drinks_format_file_contents
        _str_contents = get_data_file_contents "drinks"
        _h_drinks = {}
  
        _arr_file_contents = _str_contents.split "\n"
        _int_drink_counter = 0
        _int_item_counter = 0
  
        _arr_file_contents.each do |str_line|
          _arr_line_contents = str_line.split ","
          
  
          if _arr_line_contents.length == 1
  
            _int_item_counter = 0
            _int_drink_counter += 1
  
            _h_drinks[_int_drink_counter] =  {
              id: _int_drink_counter,
              drink_type: str_line,
              items: Hash.new
            }
          else
            _int_item_counter += 1
            _arr_items_data = str_line.split ","
  
            puts "_h_drinks: #{_h_drinks}"
  
            _h_drinks[_int_drink_counter][:items][_int_item_counter] = {
              id: _int_item_counter,
              name: _arr_items_data[0],
              price: _arr_items_data[1],
              quantity: 0
            }
          end
        end
        
        _h_drinks
      end
  
      def get_drinks_data_real
        get_drinks_format_file_contents
      end

      def get_speakers_data_real
        get_speakers_format_file_contents
      end
  
      def get_drinks_data_file_location
        Rails.root.join('public', 'uploads', 'drinks.csv')
      end
  
      def get_speaker_data_file_location
        Rails.root.join('public', 'uploads', 'speakers.csv')
      end

      def get_speakers_format_file_contents
        _str_contents = get_data_file_contents "speakers"
        _h_speakers = {}
  
        _arr_file_contents = _str_contents.split "\n"
        _int_drink_counter = 0
        _int_item_counter = 0
  
        _arr_file_contents.each_with_index do |str_line, index|

          _id = index + 1
          _arr_line_contents = str_line.split ","

          _str_range  = _arr_line_contents[0]
          _str_name   = _arr_line_contents[1]
          _str_price  = _arr_line_contents[2]
          _str_image_file_location = _arr_line_contents[3]
          _str_description = _arr_line_contents[4]

          _arr_line_contents.each
          
          _h_speakers[_id] = {
            id: _id,
            name: _str_name,
            range: _str_range,
            price: _str_price,
            description: _str_description,
            image: _str_image_file_location,
            checked: false
          }

        end
        
        _h_speakers
      end
    end
  end
end

  