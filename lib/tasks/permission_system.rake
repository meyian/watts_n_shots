namespace :permission_system do

  desc "This is used to set is_admin true/false for a user"
  # rake 'permission_system:set_admin[test@google.com, 1]'
  task :set_admin, [:user_email, :status] => :environment do |t, args|
    user = User.where(email: args.user_email).first 
    if !user.nil?
      puts user.name
      if args.status.to_i == 1
        user.update!(is_admin: true)
        puts "user is_admin: true"
      else
        user.update!(is_admin: false)
      puts "user is_admin: false"
      end
    else
      puts "user not found"
    end
        
  end
  
end
